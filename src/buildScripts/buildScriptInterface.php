<?php
/**
 * Created by PhpStorm.
 * User: JW
 * Date: 11/08/14
 * Time: 16:00
 */

namespace deathSparrow\buildPHP\buildScripts;

interface buildScriptInterface
{
    public function run($rootFolder);

    public function getExitCode();

    public function hasWarning();

    public function hasError();

    public function getWarnings();

    public function getErrors();
} 