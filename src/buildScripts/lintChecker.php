<?php


namespace deathSparrow\buildPHP\buildScripts;


class lintChecker extends buildScript implements buildScriptInterface
{
    public function run($rootFolder)
    {
        $this->lint($rootFolder);
        var_dump($this->lint($rootFolder));
    }

    public function getExitCode()
    {
        // TODO: Implement getExitCode() method.
    }

    public function hasWarning()
    {
        // TODO: Implement hasWarning() method.
    }

    public function hasError()
    {
        // TODO: Implement hasError() method.
    }

    public function getWarnings()
    {
        // TODO: Implement getWarnings() method.
    }

    public function getErrors()
    {
        // TODO: Implement getErrors() method.
    }

    /**
     * Recurses each directory and runs PHP's lint function against each file
     * to test for parse errors.
     *
     * @param    string $dir the directory you would like to start from
     * @return    array        the files that did not pass the test
     */
    protected function lint($dir = 'C:\dev\\')
    {
        static $failed = array();

        foreach (new \RecursiveDirectoryIterator($dir) as $path => $objSplFileInfo) {
            // recurse if dir
            if ($objSplFileInfo->isDir()) {
                if ($objSplFileInfo->getFileName() === '.' || $objSplFileInfo->getFileName() === '..'  ) {
                    continue;
                }

                $this->lint($objSplFileInfo->getPathName());

                continue;
            }

//            // are there any non-dirs that aren't files?
//            if (!$objSplFileInfo->isFile()) {
//                throw new UnexpectedValueException('Not a dir and not a file?');
//            }

            // skip non-php files
            if (preg_match('#\.php$#', $objSplFileInfo->getFileName()) !== 1) {
                continue;
            }

            // perform the lint check
//            $output = array();
            $result = exec('php -l ' . escapeshellarg($objSplFileInfo), $output, $returnValue);

            if (preg_match('#^No syntax errors detected in#', $result) !== 1) {
                $failed[$objSplFileInfo->getPathName()] = $result;
//                echo $failed, ' = ', $result;
//                var_dump($result);
                var_dump($returnValue);
//                die;
            }

        }

        var_dump($output);
        return $failed;
    }
} 